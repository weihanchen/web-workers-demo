function a() {
   throw new Error("error");
}

function b() {
   a();
}

function c() {
   b();
}

c();
