if (typeof (Worker) === "undefined") {
   alert("Ops, your browser doesn't support HTML5 Web Worker! Please choose another modern browser and try again.");
}
/* elements declare */
var sortBtn = document.getElementById('sort-btn');
var sortWithTimeoutBtn = document.getElementById('sort-with-timeout');
var sortWithWorkerBtn = document.getElementById('sort-with-worker');
var sizeInput = document.getElementById('size-input');
var timesInput = document.getElementById('times-input');
var resultDiv = document.getElementById('result');
var loaderDiv = document.getElementById('loader');

/* event binding */
sortBtn.addEventListener('click', sort);
sortWithTimeoutBtn.addEventListener('click', sortWithTimeout);
sortWithWorkerBtn.addEventListener('click', sortWithWorker);

loaderDiv.style.display = 'none';

/* functions */
function ascSort(sourceArr) {
   var ascArr = sourceArr.slice();
   ascArr.sort(function (a, b) {
      return a - b;
   });
}

function finished(start, end) {
   var time = end - start;
   resultDiv.innerText = time + 'ms';
   resultDiv.style.display = 'block';
   loaderDiv.style.display = 'none';
}

function loading() {
   resultDiv.style.display = 'none';
   loaderDiv.style.display = 'block';
}

function genDescArr(n) {
   var result = [];
   while (--n >= 0) {
      result.push(n);
   }
   return result;
}

function sort() {
   var size = parseInt(sizeInput.value);
   var times = parseInt(timesInput.value);
   var descArr = genDescArr(size);
   var start = performance.now();
   loading();
   while (--times >= 0) {
      ascSort(descArr);
   }
   var end = performance.now();
   finished(start, end);
}

function sortWithTimeout() {
   var size = parseInt(sizeInput.value);
   var totalTimes = parseInt(timesInput.value);
   var times = totalTimes;
   var count = 0;
   var descArr = genDescArr(size);
   var start = performance.now();
   loading();
   while (--times >= 0) {
      setTimeout(function () {
         ascSort(descArr);
         if (++count === totalTimes) {
            end = performance.now();
            finished(start, end);
         }
      });
   }
}

function sortWithWorker() {
   var size = parseInt(sizeInput.value);
   var totalTimes = parseInt(timesInput.value);
   var times = totalTimes;
   var count = 0;
   var descArr = genDescArr(size);
   var start = performance.now();
   loading();
   while (--times >= 0) {
      var worker = new Worker('worker.js');
      worker.postMessage(descArr);
      worker.onmessage = function (e) {
         if (++count === totalTimes) {
            end = performance.now();
            finished(start, end);
         }
      }
   }
}