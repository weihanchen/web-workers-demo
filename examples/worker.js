self.onmessage = function(e) {
   var descArr = e.data;
   var ascArr = ascSort(descArr);
   self.postMessage(ascArr);
}

function ascSort(sourceArr) {
   var ascArr = sourceArr.slice();
   ascArr.sort(function (a, b) {
      return a - b;
   });
   return ascArr;
}
